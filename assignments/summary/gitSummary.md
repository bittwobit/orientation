**What is Git?**

Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. Git is easy to learn and has a tiny footprint with lightning fast performance.

Let me break it down and explain the wording:


**1. Control System-** This basically means that Git is a content tracker. So Git can be used to store content-it is mostly used to store code due to the other features it provides.

**2. Version Control System-** The code which is stored in Git keeps changing as more code is added. Also, many developers can add code in parallel. So Version Control System helps in handling this by maintaining a history of what changes have happened. Also, Git provides features like *branches* , *pull*, *push* and *merges* etc

**3. Distributed Version Control System-** Git has a remote repository which is stored in a server and a local repository which is stored in the computer of each developer. This means that the code is not just stored in a central server, but the full copy of the code is present in all the developers computers. Git is a Distributed Version Control System since the code is present in every developer’s computer.

Some commands used in Git are-

1. *git init*- `git init [repository name]`
> This command is used to start a new repository.

2. *git clone*- `git clone [url]`
> This command is used to obtain a repository from an existing URL.

3. *git add*- `git add [file]`
> This command adds a file to the staging area.

4. *git commit*- `git commit -m “[ Type in the commit message]”`
> This command records or snapshots the file permanently in the version history.

5. *git status*- `git status`
> This command lists all the files that have to be committed.


                                                
![Gitlab pointers](extras/simplified-git-flow.png)

