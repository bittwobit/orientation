**What is Gitlab?**

Gitlab is a service that provides remote access to Git repositories. In addition to hosting your code, the services provide additional features designed to help manage the software development lifecycle. These additional features include managing the sharing of code between different people, bug tracking, wiki space and other tools for *social coding*. 

**Features**
1. GitLab hosts your (private) software projects for free.

2. GitLab is a platform for managing Git repositories.

3. GitLab offers free *public* and *private* repositories, *issue-tracking* and *wikis*.

4. GitLab is a user friendly web interface layer on top of Git, which increases the speed of working with Git.

**Advantages**
1. GitLab provides GitLab Community Edition version for users to locate, on which servers their code is present.

2. GitLab provides unlimited number of *private* and *public* repositories for free.

3. The Snippet section can share small amount of code from a project, instead of sharing whole project.

**Disadvantages**

1. While pushing and pulling repositories, it is not as `fast` as GitHub.

2. GitLab interface will `take time` while switching from one to another page.


**`Workflow of Gitlab`**

![Gitlab pointers](extras/gitlab-workflow.JPG)

